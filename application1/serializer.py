from rest_framework import serializers
from application1.models import Store


actions = ['plus', 'minus', 'divide', 'multiply']


class CalcSerializer(serializers.Serializer):
    action = serializers.ChoiceField(choices=actions)
    number1 = serializers.IntegerField()
    number2 = serializers.IntegerField()

    def validate_number2(self, value):
        if value == 0:
            raise serializers.ValidationError("Can`t divide by zero")
        return value


class StoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        fields = ('name',
                  'description',
                  'rate',
                  'id',
                  'owner',
                  'status')
        read_only_fields = ('status',)
