from datetime import date

from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from application1.models import Store
from application1.serializer import CalcSerializer, StoreSerializer


@api_view(http_method_names=['GET'])
def hello_world(request):
    return Response({'message': 'Hello world!'})


@api_view(http_method_names=['GET'])
def today(request):
    date_dict = {'date': date.today().strftime('%d/%m/%Y'),
                 'year': date.today().year,
                 'month': date.today().month,
                 'day': date.today().day
                 }
    return Response(date_dict)


@api_view(http_method_names=['GET'])
def my_name(request):
    name = request.query_params['name']
    return Response({"name": name})


def calc(operator, a, b):
    result = None
    try:
        if operator == "plus":
            result = a + b
        elif operator == "minus":
            result = a - b
        elif operator == "multiply":
            result = a * b
        elif operator == "divide":
            result = a / b
    except Exception as ex:
        raise APIException("calc error : " + str(ex))
    return result


@api_view(http_method_names=['POST'])
def calculator(request):
    serializer = CalcSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        result = calc(serializer.data['action'], serializer.data['number1'], serializer.data.get('number2', 0))
        return Response({'result': result})


# -----------------------------------------------------------------------------------

class StoreViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class MyStoreViewSet(ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(owner=self.request.user)
        return query_set

    def perform_create(self, serializer):
        serializer.save(**{'owner': self.request.user})

    @action(methods=['post'], detail=True)
    def mark_as_active(self, request, pk=None):
        store = self.get_object()
        store.status = 'active'
        store.save()
        serializer = self.get_serializer(store)
        return Response(serializer.data)

    @action(methods=['post'], detail=True)
    def mark_as_deactivated(self, request, pk=None):
        store = self.get_object()
        store.status = 'deactivated'
        store.save()
        serializer = self.get_serializer(store)
        return Response(serializer.data)


class AdminStoreViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['status']
    search_fields = ['name']
    ordering_fields = ['rate']

    @action(methods=['post'], detail=True)
    def mark_as_active(self, request, pk=None):
        store = self.get_object()
        if store.status == 'in_review':
            store.status = 'active'
            store.save()
        serializer = self.get_serializer(store)
        return Response(serializer.data)