from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Store(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=800)
    rate = models.IntegerField(validators=[MaxValueValidator(100), MinValueValidator(1)])
    owner = models.ForeignKey('auth.User',
                              related_name='stores',
                              null=True, blank=True,
                              on_delete=models.SET_NULL)
    status = models.CharField(
        choices=(
            ('active', 'Active'),
            ('deactivated', 'Deactivated'),
            ('in_review', 'In_review'),
        ),
        max_length=15,
        default='in_review'
    )

    class Meta:
        ordering = ['name', 'rate']