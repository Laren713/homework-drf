from django.urls import path, include
from rest_framework import routers
from application1.views import StoreViewSet, MyStoreViewSet, AdminStoreViewSet

store_router = routers.SimpleRouter()
my_store_router = routers.SimpleRouter()
admin_router = routers.SimpleRouter()
store_router.register('', StoreViewSet, basename='stores')
my_store_router.register('', MyStoreViewSet, basename='my_stores')
admin_router.register('', AdminStoreViewSet, basename='admin_stores')